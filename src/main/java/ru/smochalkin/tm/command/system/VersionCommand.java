package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractSystemCommand;

public final class VersionCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String description() {
        return "Display version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.10.0");
    }

}
