package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @NotNull
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().logout();
    }

}
