package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;

public class UserInfoCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-info";
    }

    @Override
    @NotNull
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("User id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
