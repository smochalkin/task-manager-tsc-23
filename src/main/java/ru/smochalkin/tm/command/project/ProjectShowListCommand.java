package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final List<Project> projects;
        String sortName = TerminalUtil.nextLine();
        if (sortName.isEmpty()) {
            projects = serviceLocator.getProjectService().findAll(userId);
        } else {
            @NotNull final Sort sort = Sort.getSort(sortName);
            System.out.println(sort.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sort.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index++ + ". " + project);
        }
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
