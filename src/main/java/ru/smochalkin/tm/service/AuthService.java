package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.IAuthService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.util.HashUtil;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    @NotNull
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.isLock()) throw new UserIsLocked();
        @NotNull final String hash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Override
    @Nullable
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void checkRoles(Role @Nullable ... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new UserNotFoundException();
        @Nullable final Role role = user.getRole();
        for (Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
